/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class HowManytimes {
    public static void main(String[] args) {
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int[] array1;
        int found = 0;
        array1 = new int[9];
        
        System.out.print("Array: " );
        for(int i = 0; i < array1.length; i++){
            int x = 1 + r.nextInt(50); 
            array1[i] = x;
            System.out.print(array1[i] + " ");
        }
  
        System.out.println("\n Value to find: ");
        int ans = sc.nextInt();
        
        for(int k = 0; k < array1.length; k++){
            if(ans == array1[k]){ found++;}
               
        }
          System.out.println(ans + " is found in the array " + found + " times.");
    }
}

