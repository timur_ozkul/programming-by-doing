/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import java.util.Random;

/**
 *
 * @author timur
 */
public class CopyingArrays {
    public static void main(String[] args) {
        Random r = new Random();
        int[] array1;
        int[] array2;
        array1 = new int[10];
        array2 = new int[10];
        
        
        for(int i = 0; i < 10; i++)
        {
            int genR = 1 + r.nextInt(100);
            array1[i] = genR;  
            array2[i] = array1[i];
        }

        // PRINTING 1
        System.out.print("Array 1: ");
        for(int g = 0; g < 10; g++)
        {
            if(g == 9){array1[9] = -7;}
            System.out.print(array1[g] + " ");
              
        }
        // PRINTING 2
         System.out.print("\nArray 2: ");  
         for(int k = 0; k < 10; k++)
         {
               
             System.out.print(array2[k] + " ");
             }
           
           
    }
}
