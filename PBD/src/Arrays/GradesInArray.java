/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class GradesInArray {
    public static void main(String[] args) throws IOException {
        
    Random r = new Random();
    Scanner sc = new Scanner(System.in);
    String name;
    String filename;
    int grade1 = 1 + r.nextInt(100);
    int grade2 = 1 + r.nextInt(100);
    int grade3 = 1 + r.nextInt(100);
    int grade4 = 1 + r.nextInt(100);
    int grade5 = 1 + r.nextInt(100);
    
    System.out.print("Name (first last): ");
    name = sc.nextLine();
    System.out.print("Filename: ");
    filename = sc.nextLine();
    
    System.out.println("Here are your randomly-selected grades (hope you pass): ");
     System.out.println("Grade 1: " + grade1);
     System.out.println("Grade 2: " + grade2);
     System.out.println("Grade 3: " + grade3);
     System.out.println("Grade 4: " + grade4);
     System.out.println("Grade 5: " + grade5);
     
     PrintWriter out = new PrintWriter(new FileWriter(filename + ".txt"));
     
     out.println(name);
     out.println("Grade 1: " + grade1);
     out.println("Grade 2: " + grade2);
     out.println("Grade 3: " + grade3);
     out.println("Grade 4: " + grade4);
     out.println("Grade 5: " + grade5);
     
     out.flush();
     out.close();
    
     System.out.println("Data saved in " + filename);
    }
}
