/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Hangman {

    char guessed;
    char missed;
    char currentG;
    String word;
    char[] arrayUnderS;
    List<Character> wordChar = new ArrayList();//A list of characters of the word
     


    List<Character> corrList = new ArrayList();//A list of incorrect guesses
    List<Character> missList = new ArrayList();//A list of correct guesses
;
    
    public static void main(String[] args) {
        Hangman h = new Hangman();

        int counter = 0;
         h.randomWord();
            h.arrayMaker();
        do {
           
            
            h.display();
            counter += h.guesser();
        } while (counter != h.getLength());

        System.out.println("You got It!!");
        System.out.println("Thank you for playing");

    }

    public String wordDisplay(){
     
  
        for(int i = 0; i < wordChar.size(); i++){
             for(char k: corrList){
                 
                 if(wordChar.get(i) == k){
                     arrayUnderS[i] = k;////
                 }
             }    
            }
        
      String hidenWord = new String(arrayUnderS);
            
        
      return hidenWord;
        }

    public void display() {
        System.out.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
        System.out.println(word.length() + " letter word");
        System.out.println("Word: " + wordDisplay());
        
        System.out.println("Misses: ");
        System.out.print("Guess: ");
        System.out.println(playerGuess());
        System.out.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");

    }

    public int guesser() {
        int yes = 0;
        
        for (char ch : wordChar) {
            if (currentG == ch) {
                corrList.add(currentG);
                yes = 1;
            } 
        }
        if(yes == 0){missList.add(currentG);}
        return yes;
    }
    

    public void randomWord() {
        Random r = new Random();
        int x = 1 + r.nextInt(10);
        String[] words = {"splendir", "candid", "marriott", "birdfood", "jameson", "mcdonalds", "kentucky", "louisville", "ohio", "redneck"};
        word = "james";//need to keep it ramdon later!

    }

    public char playerGuess() {

        Scanner sc = new Scanner(System.in);
        currentG = sc.next().charAt(0);
        return currentG;

    }

    public void arrayMaker() {
        
        for(int i = 0; i < word.length(); i++){
         
            wordChar.add( word.charAt(i));
        }
            
        arrayUnderS = new char[word.length()];
        
        for(int k = 0; k < word.length(); k++){
            arrayUnderS[k] = '_';
        }
          
        }
 
    public int getLength() {

        return word.length();
    }


    }
    
    
  
