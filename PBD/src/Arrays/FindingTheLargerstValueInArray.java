/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import java.util.Random;


/**
 *
 * @author apprentice
 */
public class FindingTheLargerstValueInArray {
      public static void main(String[] args) {
        Random r = new Random();
        
        int[] array1;
        array1 = new int[9];
        int prevI = 0;
       
        System.out.print("Array: " );
        for(int i = 0; i < array1.length; i++){
            int x = 1 + r.nextInt(50); 
            array1[i] = x;
            System.out.print(array1[i] + " ");
            if(array1[i] > prevI){
                prevI = array1[i];
            }
            }
        System.out.println("\nThe largest value is " + prevI);
        }
      }

