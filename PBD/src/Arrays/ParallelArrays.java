/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ParallelArrays {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        String[] name = {"Mitchell", "Oritz", "Luu", "Zimmerman", "Brooks"};
        double[] grade = {99.5, 78.5, 95.6, 96.8, 82.7};
        int[] iD = {123456, 813225, 823669, 307760, 827131};
        
        for(int i = 0; i < name.length; i++){
            System.out.println(name[i] + " " + grade[i] + " " + iD[i]);
        }
        System.out.println();
        System.out.println("ID number to find: ");
        int ans = sc.nextInt();
        for(int k = 0; k < iD.length; k++){
            if(iD[k] == ans)
            {
                System.out.println("Name " + name[k]);
                System.out.println("Grade " + grade[k]);
                System.out.println("ID " + iD[k]);
            }
        }
                
    }
}
