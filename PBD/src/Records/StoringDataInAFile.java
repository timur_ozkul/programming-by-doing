/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Records;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class StoringDataInAFile {
    public static void main(String[] args) throws IOException {
        
        Scanner sc = new Scanner(System.in);
        String carMake[];
        String carModel[];
        int carYear[];
        String carLicense[];
        carMake = new String[5];
        carModel = new String[5];
        carYear = new int[5];
        carLicense = new String[5];
   
        
        System.out.println("What is the Make of Car1: ");
        carMake[0] = sc.next();
        System.out.println("What is the Model of Car1: ");
        carModel[0] = sc.next();
        System.out.println("What is the Year of Car1: ");
        carYear[0] = sc.nextInt();
        System.out.println("What is the License of Car1: ");
        carLicense[0] = sc.next();
        
        System.out.println("What is the Make of Car2: ");
        carMake[1] = sc.next();
        System.out.println("What is the Model of Car2: ");
        carModel[1] = sc.next();
        System.out.println("What is the Year of Car2: ");
        carYear[1] = sc.nextInt();
       System.out.println("What is the License of Car2: ");
       carLicense[1] = sc.next();
        
        System.out.println("What is the Make of Car3: ");
        carMake[2] = sc.next();
        System.out.println("What is the Model of Car3: ");
        carModel[2] = sc.next();
       System.out.println("What is the Year of Car3: ");
        carYear[2] = sc.nextInt();
        System.out.println("What is the License of Car3: ");
        carLicense[2] = sc.next();
        
        System.out.println("What is the Make of Car4: ");
        carMake[3] = sc.next();
        System.out.println("What is the Model of Car4: ");
        carModel[3] = sc.next();
        System.out.println("What is the Year of Car4: ");
        carYear[3] = sc.nextInt();
        System.out.println("What is the License of Car4: ");
        carLicense[3] = sc.next();
        
        System.out.println("What is the Make of Car5: ");
        carMake[4] = sc.next();
        System.out.println("What is the Model of Car5: ");
        carModel[4] = sc.next();
        System.out.println("What is the Year of Car5: ");
        carYear[4] = sc.nextInt();
        System.out.println("What is the License of Car5: ");
       carLicense[4] = sc.next();
        
        
        System.out.println("To which file do you want to save this information? ");
        String ans = sc.next();
        
        PrintWriter out = new PrintWriter(new FileWriter(ans));
        
        for(int i = 0; i < 5; i++){
           out.println(carMake[i] + carModel[i] + carYear[i] + carLicense[i]);
            }
            
        out.flush();
        out.close();
         
        
        int holder = 0;
        for(int y = 0; y < carYear.length; y++){
             
            if(carYear[y] > carYear[y+1]){
                holder = carYear[y];
                carYear[y] = carYear[y+1];
                carYear[y+1] = holder;
                
            }
            
        }
        
        System.out.println(carYear[0] + " " + carYear[1] + " " + carYear[2] + " " + carYear[3]);
        
        
        
    }
    }
