/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Records;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class GettingDataFromFile {
    public static void main(String[] args) throws IOException {
        String breed1 = "Great Dane";
        int age = 12;
        double weight = 12.5;
        
        String breed2 = "Yorkie";
        int age2 = 2;
        double weight2 = 15;
        PrintWriter out = new PrintWriter(new FileWriter("PBDrec1.txt"));
        out.print(breed1);
        out.print(age);
        out.println(weight);
        
        out.print(breed2);
        out.print(weight2);
        out.println();
        
        out.flush();
        out.close();
        
        Scanner sc = new Scanner(new BufferedReader(new FileReader("PBDrec1.txt")));
        
        while(sc.hasNextLine()){
            String currentLine = sc.nextLine();
            System.out.println(currentLine);
        }
    }
}
