/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoWhileLoops;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class ShortDoubleDice {
    public static void main(String[] args) {
        
    Random r = new Random();
    System.out.println("LETS ROLL THE DICE");
    int i = 0;
    do{
    int x = 1 + r.nextInt(6);
    int y = 1 + r.nextInt(6);
    System.out.println("Roll #1: " + x + " ");
    System.out.println("Roll #2: " + y + " ");
    System.out.println("The total is: " + (x + y));
    i++;
    }while(i < 3);
}
    
}
