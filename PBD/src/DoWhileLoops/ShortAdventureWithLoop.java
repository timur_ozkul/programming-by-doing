/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoWhileLoop;

import java.util.Scanner;

/**
 *
 * @author timur
 */
public class ShortAdventureWithLoop {
    
		  public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
    
		int nextroom = 1;
		String choice = "";

		
		{
			if ( nextroom == 1 )
			{
				System.out.println( "You are in a coffin want to kick your way out or headbut your way out." );
				System.out.print( "> " );
				choice = keyboard.nextLine();
				if ( choice.equals("kick") )
					nextroom = 2;
				else if ( choice.equals("headbut") )
					nextroom = 3;
				else
					System.out.println( choice + " wasn't one of the options. Try again." );
			}
			if ( nextroom == 2 )
			{
				System.out.println( "Your out you find yourself in an office in the middle of the night, there is only on thing to do go out the door" );
				System.out.print( "> " );
				choice = keyboard.nextLine();
				if ( choice.equals("door") )
					nextroom = 3;
				else
					System.out.println( choice + " wasn't one of the options. Try again." );
			}
			if ( nextroom == 3 )
			{
				System.out.println( "You find yourself in a concrete hallway. Oddly, there is only a single" );
				System.out.println( "\"door\" visible. Otherwise, the hall just extends about fifteen feet" );
				System.out.println( "in either direction, and ends in a smooth, blank, concrete wall." );
				System.out.println( "Do you want to enter the \"door\" or approach the \"wall\" looking for clues?" );
				choice = keyboard.nextLine();
				System.out.print( "> " );
				if ( choice.equals("door") )
					nextroom = 5;
				else if ( choice.equals("wall") )
					nextroom = 4;
				else
					System.out.println( choice + " wasn't one of the options. Try again." );
			}
			if ( nextroom == 4 )
			{
				System.out.println( "Upon closer inspection, the seemingly blank wall shimmers ever so slightly" );
				System.out.println( "in the dim light. You put forward a tentative hand, and it pushes through," );
				System.out.println( "a feeling of static sliding up your arm." );
				System.out.println();
				System.out.println( "You pass through the portal into the unknown...." );
				nextroom = 0;
			}
                        if ( nextroom == 5 )
			{
				System.out.println( "There in the new room you find a tv and couch and fat man eating chips and watching tv" );
				System.out.println( "As you approach him he waves his hand, and black hole opens up under you and you fall through to the unknown..." );
				System.out.println( "a feeling of static sliding up your arm." );
				
				nextroom = 0;
			}
				
		

		System.out.println( "\nThe game is over. The next episode can be downloaded for only 800 Microsoft points!" );
	}
	
}
}