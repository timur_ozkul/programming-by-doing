/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoWhileLoop;

import java.util.Scanner;


/**
 *
 * @author timur
 */
public class SafeSquareRoot {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        System.out.print("SQUARE ROOT \n Enter a number: ");
        int x = sc.nextInt();
        double xS = Math.sqrt(x);
        System.out.print("The square root of " + x + " is " + xS);
        
    }
    
}
