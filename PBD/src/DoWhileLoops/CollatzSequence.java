/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoWhileLoop;

import java.util.Scanner;

/**
 *
 * @author timur
 */
public class CollatzSequence {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int ans;
        System.out.print("Starting number: ");
        ans = sc.nextInt();
        
        do{
        
        
        if(ans % 2 == 0){
            
            ans = ans/2;
        }else{
            ans = ( ans * 3 ) + 1;
        }
        System.out.print(ans + " ");
        }while(ans != 1);
    }
}

