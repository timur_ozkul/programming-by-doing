/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoWhileLoops;
import java.util.Random;
import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class FlipAgain {
    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        
        Random r = new Random();
        int x =  r.nextInt(1);
        String coin, answer;
        
        do{
            if(x == 1){
                coin = "head";
            }else{
                coin = "tails";
            }  
            System.out.println("You flip a coin and it is.. " + coin);
            System.out.println("You want to play again y/n");
            answer = sc.nextLine();
          }while("y".equals(answer)); //does the == only for numbers?
    }
    
}
