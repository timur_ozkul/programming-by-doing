/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoWhileLoop;

import java.util.Scanner;

/**
 *
 * @author timur
 */
public class RightTriangleChecker {
    public static void main(String[] args) {
    
    int s2, s3;    
    Scanner sc = new Scanner(System.in); 
    System.out.print("Enter three integers: \nSide 1: ");
    int s1 = sc.nextInt(); //why is scanner new scanner not on each individual line
   
    do{
    System.out.print("Side 2: ");
    s2 = sc.nextInt();
    if(s1 >= s2){
     System.out.println(s2 + " is smaller than " + s1 + ". Try again.");   
    }
    }while (s2 <= s1);
    
    do{
    System.out.print("Side 3: "); 
    s3 = sc.nextInt();
    if(s3 <= s2){
      System.out.println(s3 + " is smaller than " + s2 + ". Try again.");  
    }
    }while (s3 <= s2);
    System.out.println("Your three side are: " + s1 + " " + s2 + " " + s3);
    if(s1 < s2 && s2 <s3){
    System.out.println(" These sides do make a right triangle.  Yippy-skippy!");
    }else{
      System.out.println(" NO!  These sides do not make a right triangle! ");
    }
    
}}
