/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoWhileLoop;

import java.util.Scanner;

/**
 *
 * @author timur
 */
public class BabyNim {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        int pile1 = 3;
        int pile2 = 3;
        int pile3 = 3;
       
        int ansAmount;
        
        while(pile1 > 0 || pile2 > 0 || pile3 > 0){
        System.out.println("A: " + pile1 + "\t B: " + pile2 + "\t C: " + pile3);
        System.out.println("1 for A \t 2 for B \t 3 for C ");
        System.out.print("Choose a pile: ");
        int ans = sc.nextInt();//have to have both next line or both int
        System.out.print("How many to remove from pile " + ans + ": ");
        ansAmount = sc.nextInt();
        
            if(ans == 1){ pile1 = pile1 - ansAmount;}
            if(ans == 2){ pile2 = pile2 - ansAmount;}
            if(ans == 3){ pile3 = pile3 - ansAmount;}
          
        }
    }}
