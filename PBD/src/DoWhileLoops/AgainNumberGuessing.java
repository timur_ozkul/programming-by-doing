/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoWhileLoops;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AgainNumberGuessing {
    public static void main(String[] args) {
       Random r = new Random();
       Scanner sc = new Scanner(System.in);
       System.out.println("I have choosen a number between 1 to 10, try to guess it! ");
       int x = 1 + r.nextInt(10);
       int guess;
       int i = 0;
       do{
         i++;  
        guess = sc.nextInt();
        System.out.println("Your guess was: " + guess);
        if(guess == x){System.out.println("You got it!");}else{System.out.println("Wrong!!  Guess again!");}
       }while(guess != x);
       System.out.println("It only took " + i + " tries.");
    }
}
