/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WhileLoops;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class HiLoLimited {
      public static void main(String[] args) {
        
    
    Scanner sc = new Scanner(System.in);
    Random r = new Random();
    int x = 1 + r.nextInt(100);
    int guess = 0;
    int looper = 0;
    
    while(looper < 7){
    System.out.println("I'm Thinking of a number between 1-100. Try to guess it. ");
    guess = sc.nextInt();
    if(guess > x){
        System.out.println("Sorry, you are too high.");
    }else if(guess < x){
        System.out.println("Sorry, you are too low.");
    }else{
        System.out.println("You guessed it! What are the odds?!?");
        looper = 20;}
    looper++;
    }
    if(looper > 6 && looper < 20){System.out.println("Sorry, you didn't guess it in 7 tries.  You lose");}
}
}
