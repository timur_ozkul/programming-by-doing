/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WhileLoops;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class CounterGuessingWithCounter {
    
    public static void main(String[] args) {
          
      
    Random r = new Random();
    Scanner sc = new Scanner(System.in);
    int guessedNo, counter =0;
    
    int x = 1 + r.nextInt(10);
    
    do{
            System.out.println("I'm thinking of a number between 1 to 10. What number is it? \n Whats your guess:");
            guessedNo = sc.nextInt();
        if(x != guessedNo){
            System.out.println("Wrong!");
        }
        counter ++;
    }while(guessedNo != x);
    System.out.println("That's right!  You're a good guesser. \n It only took you " + counter + " tries."); 
    
      }
}
