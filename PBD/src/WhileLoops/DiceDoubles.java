/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WhileLoops;
import java.util.Random;
/**
 *
 * @author apprentice
 */
public class DiceDoubles {
    public static void main(String[] args) {
        Random r = new Random(); 
        int x1 = 1 + r.nextInt(6);
        int x2 = 1 + r.nextInt(6);
                
        System.out.println("Roll #1: " + x1);
        System.out.println("Roll #2: " + x2);
        System.out.println("The total is: " + (x1 + x2));
    }
}
