/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WhileLoops;

import java.util.Scanner;

/**
 *
 * @author timur
 */
public class AddingValuesinLoop {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int total =0;
        int ans = 1;
        
        System.out.println("I will add up the numbers you give me.");
        while(ans != 0){
            System.out.print("Number: ");
            ans = sc.nextInt();
            total = total + ans;
            System.out.println("The total so far is " + total);
        }
        
        
    }
}
