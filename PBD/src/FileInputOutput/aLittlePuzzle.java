/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileInputOutput;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class aLittlePuzzle {
       public static void main(String[] args) throws FileNotFoundException {

        Scanner sc = new Scanner(System.in);

        System.out.println("Do you want to read file 1 or 2?");
        int choice = sc.nextInt();

        String file = "";

        switch (choice) {

            case 1:
                file = "LittlePuzzle.txt";
                break;
            case 2:
                file = "LittlePuzzle2.txt";
                break;
            default:
                System.out.println("Not correct input");
                System.exit(0);
        }

        Scanner fromFile = new Scanner(new BufferedReader(new FileReader(file)));

        String letters = "";

        while (fromFile.hasNextLine()) {

            letters = letters + fromFile.nextLine();

        }

        String cletters = "";

        for (int i = 0; i < letters.length(); i++) {

            if ((i + 1) % 3 == 0) {

                cletters = cletters + letters.charAt(i);

            }

        }

        System.out.println(cletters);

    }
}
