/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileInputOutput;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class SummingThreeNumbersfromFile {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("3nums.txt")));
        int total = 0;
        int x = 0;
        int k = 0;
        String currentLine;
        
        while (sc.hasNextLine()) { //does it have a next line go to the next iteration
            currentLine = sc.nextLine(); //stores the string in that line
            x = Integer.parseInt(currentLine);
            total = total + x;
            System.out.println(currentLine);
            
        }
        System.out.println("The total of the given numbers is: " + total);
    }
}
