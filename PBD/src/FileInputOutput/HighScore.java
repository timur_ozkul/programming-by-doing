/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileInputOutput;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class HighScore {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        
        
        System.out.println("You got a high score!");
        System.out.print("Please enter your score: ");
        int ans1 = sc.nextInt();
        System.out.print("Please enter your name: ");
        String ans2 = sc.next();
        System.out.print("Data stored into score.txt");
        
        PrintWriter out = new PrintWriter(new FileWriter("score.txt"));
        out.print(ans2 + " has a highscore of " + ans1);
        
        out.flush();
        out.close();
        
    }
        
        
    }
    

