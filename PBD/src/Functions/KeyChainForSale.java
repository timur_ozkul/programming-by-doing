/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functions;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class KeyChainForSale {
    int keyCNo = 0;
    Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args) {
        KeyChainForSale kc = new KeyChainForSale();
        Scanner sc = new Scanner(System.in);
        int exitSign = 1;

        
        System.out.println("Ye Olde Keychain Shoppe");
        kc.options();
        
        while(exitSign != 0){
         
        int ans = sc.nextInt();
        
        if(ans == 1){kc.keyCOrder();}
        else if(ans == 2){kc.remKey();}
        else if(ans == 3){kc.viewOrd(); kc.options();}
        else if(ans == 4){kc.checkout(); exitSign = 0;}
        else{System.out.println("Error repeat please");}
         }//while
        
        System.out.println("What is your name?");
        String name = sc.next();
        kc.viewOrd();
//        System.out.println("You have " + kc.keyCNo + " keychains.");
//        System.out.println("Keychains cost $10 each.");
//        System.out.println("Total cost is $" + (kc.keyCNo * 10) + ".");
        System.out.println("Thanks for your order, " + name + "!");
       

        
    }
    
    public void keyCOrder(){
        int keyCNoAdd;
        System.out.println();
        System.out.println(" ---ADD KEYCHAINS--- ");
        System.out.print("You have " + keyCNo + " keychains. How many to add? ");
        keyCNoAdd = sc.nextInt();
        keyCNo = keyCNo + keyCNoAdd;
        System.out.println("You now have " + keyCNo + " keychains");
        
        options();
        
    }
    
    public void remKey(){
        int keyCRemover;
        System.out.println();
        System.out.println(" ---REMOVE KEYCHAINS--- ");
        keyCRemover = errorCheck();
        keyCNo = keyCNo - keyCRemover;
        
        options();
        
         }
        
    public void viewOrd(){
        double salesTax = 1.0825;
        double shippingCost = 5;
        double keyChainShippingCost = 1;
        double totalShipping = (shippingCost + (keyChainShippingCost * keyCNo));
        double total = ((totalShipping + (keyCNo * 10)) * salesTax);
        System.out.println();
        System.out.println(" ---VIEW ORDER--- ");
        System.out.println("You have " + keyCNo + "keychains.");
        System.out.println("Keychains cost $10 each");
        System.out.println("Total cost is $" + (keyCNo * 10) + ".");
        System.out.println("The total shipping cost is. $" + totalShipping);
        System.out.println("The total is with tax: $" + total);
        
        
        
    }
    
    public void checkout(){
        System.out.println("CHECKOUT");
    }
    
    public void options(){
        
        System.out.println();
        System.out.println("1. Add Keychains to order");
        System.out.println("2. Remove Keychains from Order");
        System.out.println("3. View Current Order");
        System.out.println("4. Checkout");
        System.out.print("Please enter your choice: "); 
    }
    
    public int errorCheck() {
        int keyCRemover;
        do{
            System.out.print("You have " + keyCNo + " keychains. How many to remove? ");
            keyCRemover = sc.nextInt();
            if( 0 > (keyCNo - keyCRemover)){
             System.out.println("You cant remove more than you have ");   
            }
        }while((keyCNo - keyCRemover) < 0 );
        return keyCRemover;
    }
}
