/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functions;

import java.util.Scanner;

/**
 *
 * @author timur
 */
public class AreaCalculator {
    public static void main(String[] args) 
    {
        Scanner sc = new Scanner(System.in);
        int base, height, length, width, sideLength, radius, loopCounter =0;
        double area;
        while(loopCounter < 1){
        System.out.println("1) Triangle \n2) Rectangle \n3) Square \n4) Circle \n5) Quit");
        System.out.print("Which shape: ");
        int ans = sc.nextInt();
 
        if(ans == 1)
        { //TRIANGLE
            System.out.print("Base: ");
            base = sc.nextInt();
            System.out.print("Height: ");
            height = sc.nextInt();
            area = area_triangle(base, height);
            System.out.println("The area is " + area);
        }else if(ans == 2)
        {// RECTANGLE
            System.out.print("Length: ");
            length = sc.nextInt();
            System.out.print("Width: ");
            width = sc.nextInt();
            area = area_rectangle(length, width);
            System.out.println("The area is " + area);
        }else if(ans == 3)
        { // SQUARE
            System.out.print("Side Length: ");
            sideLength = sc.nextInt();
           area = area_square(sideLength);
            System.out.println("The area is " + area);
        }else if(ans == 4)
        { // CIRCLE
            System.out.print("Radius: ");
            radius = sc.nextInt();
            area = area_circle(radius);
            System.out.println("The area is " + area);
        }else if(ans == 5)
        { //QUIT
            System.out.print("Goodbye. ");
            loopCounter = 5;
        }else{
            System.out.print("Error");
        }}
        
    }
    
    //          AREA CALCULATIONS
public static double area_circle( int radius )
{
    double a = Math.pow((Math.PI * radius), 2);
    return a;
}
public static double area_rectangle( int length, int width )
{
    double a = length * width;
            return a;
}
public static double area_square( int side ) 
{
    double a = Math.pow(side, 2);
     return a;
}
public static double area_triangle( int base, int height )
{
    double a =(base * height)/2;
    return a;
}
}
