/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functions;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class DisplaySomeMultiples {
    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
       
       System.out.print("Choose a number: ");
       int ans = sc.nextInt();
       
       for(int i = 0; i < 13; i++){
           int result = i * ans;
           System.out.println(i + " x " + ans + " = " + result);
       }
           
       
    }
}
