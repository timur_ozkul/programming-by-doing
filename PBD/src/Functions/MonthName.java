/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functions;

/**
 *
 * @author timur
 */
public class MonthName {
    public static void main(String[] args) 
    {
    System.out.println( "Month 1: " + month_name(1) );
    System.out.println( "Month 2: " + month_name(2) );
    System.out.println( "Month 3: " + month_name(3) );
    System.out.println( "Month 4: " + month_name(4) );
    System.out.println( "Month 5: " + month_name(5) );
    System.out.println( "Month 6: " + month_name(6) );
    System.out.println( "Month 7: " + month_name(7) );
    System.out.println( "Month 8: " + month_name(8) );
    System.out.println( "Month 9: " + month_name(9) );
    System.out.println( "Month 10: " + month_name(10) );
    System.out.println( "Month 11: " + month_name(11) );
    System.out.println( "Month 12: " + month_name(12) );
    System.out.println( "Month 43: " + month_name(43) );
        
    }
    public static String month_name(int month)
    {
        String monthS = "";
        switch (month){
            case 1:
                monthS = " January";
                break;
            case 2:
                monthS = " February";
                break;
            case 3:
                monthS = " March";
                break;
            case 4:
                monthS = " April";
                break;
            case 5:
                monthS = " May";
                break;
            case 6:
                monthS = " June";
                break;
            case 7:
                monthS = " July";
                break;
            case 8:
                monthS = " August";
                break;
            case 9:
                monthS = " September";
                break;
            case 10:
                monthS = " October";
                break;
            case 11:
                monthS = " November";
                break;
            case 12:
                monthS = " December";
                break;
            default:
                System.out.println("Error");
               
        }
         return monthS;
    }
}
