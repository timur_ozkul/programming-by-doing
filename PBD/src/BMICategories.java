
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class BMICategories {
        public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Float heightS, weightS;
        
        System.out.println("Your height in Meters:");
        heightS = sc.nextFloat();
        System.out.println("Your weight in kg:");
        weightS = sc.nextFloat();
        float finalans = (weightS/heightS);
        System.out.println("Your BMI is " + finalans);
        
        if(finalans > 30){System.out.println("obese");}
        if(finalans > 25 && finalans < 29.9){System.out.println("overweight");}
        if(finalans > 18.5 && finalans < 24.9){System.out.println("normal weight");}
        if(finalans < 18.5){System.out.println("underweight");}
    }
}
