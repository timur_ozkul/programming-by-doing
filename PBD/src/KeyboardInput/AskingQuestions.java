/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeyboardInput;
import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class AskingQuestions {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scannerInput = new Scanner(System.in);
        
        String age, heightFeet, heightInches, weight;
        
        System.out.println("How old are you: ");
        age = scannerInput.nextLine();
        System.out.println("What your height in feet: ");
        heightFeet = scannerInput.nextLine();
        System.out.println("Whats your height in inches");
        heightInches = scannerInput.nextLine();
        System.out.println("What is your weight: ");
        weight = scannerInput.nextLine();
        
        System.out.println("You are " + age + " years old. You are " + heightFeet + " foot " + heightInches + " inches tall. You weigh" + weight + " pounds.");
     
    }
    
}
