/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeyboardInput;
import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class AgeFiveYears {
    
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String name, ageS;
        Byte age;
        
        System.out.println("Hello, What is your name?");
        name = sc.nextLine();
        System.out.println("Hello, " + name + "! How old are you?");
        ageS = sc.nextLine();
        age = Byte.parseByte(ageS);
        System.out.println("Did you know that in five years you will be " + (age + 5) + " years old?");
        System.out.println("And five years ago you were " + (age - 5) + "! Imagine that!");
    }
}
