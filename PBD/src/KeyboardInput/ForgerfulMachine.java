/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeyboardInput;
import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class ForgerfulMachine {
    
   public static void main(String[] args){
    Scanner sc = new Scanner(System.in);
    
    System.out.println("Give me a word!");
    sc.nextLine();
    
    System.out.println("Give me a second word! ");
    sc.nextLine();
    
    System.out.println("Great, now your favorite number? ");
    sc.nextLine();
    
    System.out.println("And your second-favorite number... ");
    sc.nextLine();
    
    System.out.println("Whew! Wasn't that fun? ");
   }
}
