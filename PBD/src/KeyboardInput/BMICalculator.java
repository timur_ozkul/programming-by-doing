/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeyboardInput;
import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class BMICalculator {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Float heightS, weightS;
        
        System.out.println("Your height in Meters:");
        heightS = sc.nextFloat();
        System.out.println("Your weight in kg:");
        weightS = sc.nextFloat();
        
        System.out.println("Your BMI is " + (weightS/heightS));
    }
}
