/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeyboardInput;
import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class ADumbCalculator {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String firstNoS, secondNoS, thirdNoS;
        Float firstNo, secondNo, thirdNo, total;
        
        System.out.println("What is your first number?");
        firstNoS = sc.nextLine();
        System.out.println("What is your second number?");
        secondNoS = sc.nextLine();
        System.out.println("What is your third number?");
        thirdNoS = sc.nextLine();
        
        firstNo = Float.parseFloat(firstNoS);
        secondNo = Float.parseFloat(secondNoS);
        thirdNo = Float.parseFloat(thirdNoS);
        
        total = (firstNo + secondNo + thirdNo)/2;
        System.out.println(total);
    }
            
}
