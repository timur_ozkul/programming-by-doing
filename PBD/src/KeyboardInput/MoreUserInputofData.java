/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeyboardInput;
import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class MoreUserInputofData {
    
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String firstName, lastName, login;
        Byte grade;
        Short student;
        Float gpa;
        
        System.out.println("Please enter the following information");
        System.out.println("First Name: ");
        firstName = sc.nextLine();
        System.out.println("Last Name: ");
        lastName = sc.nextLine();
        System.out.println("Grade: ");
        grade = sc.nextByte();
        System.out.println("Student ID: ");
        student = sc.nextShort();
        System.out.println("Login: ");
        login = sc.next();
        System.out.println("GPA: ");
        gpa = sc.nextFloat();
        
        System.out.println("Your information");
        System.out.println("First Name: " + firstName);
        System.out.println("Last Name: " + lastName);
        System.out.println("Grade: " + grade);
        System.out.println("Student ID: " + student);
        System.out.println("Login: " + login);
        System.out.println("GPA: " + gpa);
             
    }
}
