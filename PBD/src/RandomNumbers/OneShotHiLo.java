/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RandomNumbers;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class OneShotHiLo {
    public static void main(String[] args) {
        
    
    Scanner sc = new Scanner(System.in);
    Random r = new Random();
    int x = 1 + r.nextInt(100);
    
    System.out.println("I'm Thinking of a number between 1-100. Try to guess it. ");
    int guess = sc.nextInt();
    if(guess > x){
        System.out.println("Sorry, you are too high. I was thinking of " + x);
    }else if(guess < x){
        System.out.println("Sorry, you are too low. I was thinking of " + x);
    }else{
        System.out.println("You guessed it! What are the odds?!?");
    }
}
}