/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RandomNumbers;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author timur
 */
public class ThreeCardMonte {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        int x = 1 + r.nextInt(3);
        System.out.println("You slide up to Fast Eddie's card table and plop down your cash.");
        System.out.println("He glances at you out of the corner of his eye and starts shuffling.");
        System.out.println("He lays down three cards.");
        System.out.println("Which one has the ace?");
        System.out.println("\t ## \t ## \t ##");
        System.out.println("\t ## \t ## \t ##");
        System.out.println("\t 1  \t 2  \t 3");
        int ans = sc.nextInt();
        
        //THE ANSWERS
        if(ans != x)
        { System.out.println("Ha! Fast Eddie wins again! The ace was card number");}
        else{
        System.out.println("You nailed it! Fast Eddie reluctantly hands over your winnings, scowling.");
        if(ans == 1){
        System.out.println("\t AA \t ## \t ##");
        System.out.println("\t AA \t ## \t ##");
         System.out.println("\t 1  \t 2  \t 3");
        }else if(ans == 2){
        System.out.println("\t ## \t AA \t ##");
        System.out.println("\t ## \t AA \t ##");
        System.out.println("\t 1  \t 2  \t 3");
        } else if(ans == 3){
        System.out.println("\t ## \t ## \t AA");
        System.out.println("\t ## \t ## \t AA");
        System.out.println("\t 1  \t 2  \t 3");
        }
        
        
    }
}}
