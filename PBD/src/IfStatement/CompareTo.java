/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IfStatement;

/**
 *
 * @author apprentice
 */
public class CompareTo {
  public static void main ( String[] args )
  {
    // BEGINING
    System.out.print("Comparing \"axe\" with \"dog\" produces ");
    System.out.println( "axe".compareTo("dog") );

    System.out.print("Comparing \"sat\" with \"silly\" produces ");
    System.out.println( "sat".compareTo("silly") );

    System.out.print("Comparing \"susan\" with \"tobby\" produces ");
    System.out.println( "susan".compareTo("tobby") );

    System.out.print("Comparing \"mike\" with \"pipen\" produces ");
    System.out.println( "mike".compareTo("pipen") );

    System.out.print("Comparing \"rack\" with \"moon\" produces ");
    System.out.println( "rack".compareTo("moon") );

    
    System.out.print("Comparing \"applebee's\" with \"apple\" produces ");
    System.out.println( "applepie".compareTo("ape") );

    System.out.print("Comparing \"llama\" with \"drama\" produces ");
    System.out.println( "lime".compareTo("drone") );

    System.out.print("Comparing \"clean\" with \"cleaner\" produces ");
    System.out.println( "plean".compareTo("cleaners") );

    System.out.print("Comparing \"rat\" with \"dark\" produces ");
    System.out.println( "rat".compareTo("dark") );

    System.out.print("Comparing \"miketisenisamazing\" with \"fight\" produces ");
    System.out.println( "miketisenisamazing".compareTo("fight") );

    
    System.out.print("Comparing \"sad\" with \"sad\" produces ");
    System.out.println( "sad".compareTo("sad") );

    System.out.print("Comparing \"lava\" with \"lava\" produces ");
    System.out.println( "lava".compareTo("lava") );

  }
    
}
