/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IfStatement;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class HowOldAreYouSpec {
    public static void main(String[] args) {
          Scanner scr = new Scanner(System.in);
            Byte age;
        System.out.println("Whats your name?");
        String name = scr.nextLine();
        System.out.println("How old are you?");
        age = Byte.parseByte(scr.nextLine());
       
        if(age > 20){
            System.out.println("You can do whatever you like! " + name);
        }
        else if(age > 17){
            System.out.println("You are old enough to go to military " + name);
        }else if(age > 15){
            System.out.println("You are old enough to drive " + name);
        }else{
             System.out.println("You are to young go home! " + name); 
        }
    }
}
