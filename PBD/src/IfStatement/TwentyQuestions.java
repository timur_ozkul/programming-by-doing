/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IfStatement;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class TwentyQuestions {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("TWO QUESIONS! ");
        System.out.println("Think of an object, and I'll try to guess it.");
       
        System.out.println("1) Is it animal, vegetable, or mineral? ");
         String ans1 = sc.nextLine();
        System.out.println("2) Is it bigger than a breadbox ");
        String ans2 = sc.nextLine();
        
        String answer = "";
        if(ans1.equalsIgnoreCase("animal")){
            if(ans2.equalsIgnoreCase("yes")){
                answer = "dog";
            }else{answer = "mouse";}
        }else if(ans1.equalsIgnoreCase("vegetable")){
            if(ans2.equalsIgnoreCase("yes")){
                answer = "watermelon";
            }else{answer = "carrot";}
        }else if(ans1.equalsIgnoreCase("mineral")){
            if(ans2.equalsIgnoreCase("yes")){
                answer = "camaro";
            }else{answer = "paperclip";}
        }
        
        System.out.println("My guess is that you are thinking of a " + answer);
        System.out.println("I would ask you if I'm right, but I dont actually care.");
        
    }
}
