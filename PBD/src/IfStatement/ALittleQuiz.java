/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IfStatement;

import java.util.Scanner;

/**
 *
 * @author timur
 */
public class ALittleQuiz {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
         int score = 0;
         
        System.out.println("Are you ready for a Quiz? \nOkay here it comes! ");
        System.out.println("Q1) what is the capital of Alaska? \n\t1) Melbourne \n\t2) Anchorage \n\t3) Juneau");
        int ans1 = sc.nextInt();
        if(ans1 == 3){ System.out.println("That right!"); score++;}else{System.out.println("Thats wrong");}
        System.out.println("Q2) Can you store the value 'cat' in a variable of type int? \n\t 1)yes \n\t 2) no");
        int ans2 = sc.nextInt();
        if(ans2 == 2){ System.out.println("That right!");score++;}else{System.out.println("Thats wrong");}
        System.out.println("Q3) What is the result of 9+6/3? \n\t 1) 5 \n\t 2) 11 \n\t 3) 15/3");
        int ans3 = sc.nextInt();
         if(ans3 == 2){ System.out.println("That right!");score++;}else{System.out.println("Thats wrong");}
         
        
         System.out.println("Overall, you got " + score + " out of 3 correct \nThanks for playing!");
    }
}
