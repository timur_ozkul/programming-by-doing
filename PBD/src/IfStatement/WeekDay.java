/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IfStatement;
import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class WeekDay {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Pick a number of the week: ");
        int noOfDay = sc.nextInt();
        
        
        if(noOfDay == 1){
            System.out.println("Sunday");
        }else if(noOfDay == 2){
            System.out.println("Monday");
        }else if(noOfDay == 3){
            System.out.println("Tuesday");
        }else if(noOfDay == 4){
            System.out.println("Wensday");
        }else if(noOfDay == 5){
            System.out.println("Thursday");
        }else if(noOfDay == 6){
            System.out.println("Friday");
        }else if(noOfDay == 7){
            System.out.println("Saturday");
        }else{
            System.out.println("Error");
        }
        
    }
}
