/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IfStatement;

import java.util.Scanner;
import static javafx.beans.binding.Bindings.or;

/**
 *
 * @author apprentice
 */
public class TwoMoreQuestions {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Question 1: Does it belong inside or outside or both?");
        String ans1 = sc.nextLine();
        System.out.println("Question 2: Is it alive?");
        String ans2 = sc.nextLine();
        String aaa = "";
        
        if(ans1.equals("inside") && ans2.equals("yes")){
            aaa = "houseplant";
        }
        if(ans1.equals("outside") && ans2.equals("yes")){
            aaa = "bison";
        }
        if(ans1.equals("inside") && ans2.equals("no")){
            aaa = "shower curtain";
        }
        if(ans1.equals("outside") && ans2.equals("no")){
            aaa = "billboard";
        }
    }
}
