/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IfStatement;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class SpaceBoxing {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double novoWeight =0, planet;
         
        System.out.println("Please enter your current earth weight: ");
        double weight = sc.nextInt();
        System.out.println("I have information for the following planets: ");
        System.out.println("1. Venus \t 2. Mars \t 3. Jupiter \n 4. Saturn \t 5. Uranus \t 6. Neptune");
        System.out.println("Which planet are you visiting? ");
        planet = sc.nextInt();
        
        if(planet == 1){
            novoWeight = (0.78 * weight);
        }else if(planet == 2){
            novoWeight = (0.39 * weight);
        }else if(planet == 3){
            novoWeight = (2.65 * weight);
        }else if(planet == 4){
            novoWeight = (1.17 * weight);
        }else if(planet == 5){
            novoWeight = (1.05 * weight);
        }else if(planet == 6){
            novoWeight = (1.23 * weight);
        }else{
            System.out.println("You have not choosen amongs the possible answers");
        }
        
         System.out.println("Your weight would be " + novoWeight +  " pounds on that planet.");

    }
}
