/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IfStatement;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AgeMessages3 {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Your name: ");
        String ans1 = sc.nextLine();
        System.out.println("You age: ");
        int ans2 = sc.nextInt();
        
        if(ans2 < 18 && ans2 > 15){
            System.out.println("You can drive but not vote");
            
        }
        if(ans2 > 17 && ans2 < 25){
            System.out.println("You can vot but not rent a car");
        }
        if(ans2 > 25){
            System.out.println("You can do pretty much anyting");
        }
        if(ans2 < 16){
            System.out.println("You cant drive");
        }
        
    }
}
