/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ForLoop;

import java.util.Scanner;

/**
 *
 * @author timur
 */
public class AddingValuesWithForLoop {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Number: ");
        int x = sc.nextInt();
        int total = 0;
        
        for(int i = 0; i < (x + 1); i++)
        {
            System.out.print(i + " ");
            total = total + i;
        }
        System.out.println();
        System.out.print("The sum is " + total);
    }
}
