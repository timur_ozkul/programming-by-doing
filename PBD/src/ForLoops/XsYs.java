/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ForLoop;

/**
 *
 * @author timur
 */
public class XsYs {
    public static void main(String[] args) {
        double x = -10, y = 0;
         System.out.println("x \t y \n----------------");
        for(int i = 0; i < 40; i++){
            x = x + 0.5;
            y = x * x;
            System.out.println(x + " \t " + y);
        }
}}
