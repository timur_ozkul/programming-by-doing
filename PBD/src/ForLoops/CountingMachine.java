/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ForLoops;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class CountingMachine {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose you number");
        int noChoosen = sc.nextInt();
        for(int i = 0; i < (noChoosen +1); i++){
            System.out.print(i + " ");
        }
    }
}
